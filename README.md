# iOSSwiftThaiAddress

[![CI Status](https://img.shields.io/travis/nattaponpop/iOSSwiftThaiAddress.svg?style=flat)](https://travis-ci.org/nattaponpop/iOSSwiftThaiAddress)
[![Version](https://img.shields.io/cocoapods/v/iOSSwiftThaiAddress.svg?style=flat)](https://cocoapods.org/pods/iOSSwiftThaiAddress)
[![License](https://img.shields.io/cocoapods/l/iOSSwiftThaiAddress.svg?style=flat)](https://cocoapods.org/pods/iOSSwiftThaiAddress)
[![Platform](https://img.shields.io/cocoapods/p/iOSSwiftThaiAddress.svg?style=flat)](https://cocoapods.org/pods/iOSSwiftThaiAddress)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iOSSwiftThaiAddress is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
source 'https://ExcelentePop@bitbucket.org/ExcelentePop/privatepodspecs.git'

pod 'iOSSwiftThaiAddress'
```

## Author

nattaponpop, excelentepop@gmail.com

## License

iOSSwiftThaiAddress is available under the MIT license. See the LICENSE file for more info.
