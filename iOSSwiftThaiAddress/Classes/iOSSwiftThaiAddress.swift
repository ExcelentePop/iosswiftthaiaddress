//
//  ThaiAddressPack.swift
//  ThaiAddressPack
//
//  Created by apple on 11/1/2564 BE.
//

import Foundation
//import ThaiAddress
//
public class iOSSwiftThaiAddress {
    public static var bundle: Bundle = {
        let podBundle = Bundle(for: iOSSwiftThaiAddress.self)
        let bundleURL = podBundle.url(forResource: "iOSSwiftThaiAddress", withExtension: "bundle")
        return Bundle(url: bundleURL!)!
    }()
}
