//
//  AppDelegate.swift
//  iOSSwiftThaiAddress
//
//  Created by nattaponpop on 03/22/2019.
//  Copyright (c) 2019 nattaponpop. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        registerForPushNotifications()
        
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return true
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print full message.
        print(userInfo)
//        let _ = T2PKycSdk.onFCMReceived(message: userInfo)
        fcmApsHadler(aps: userInfo)
        
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response:UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
    }
}

extension AppDelegate: MessagingDelegate {
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")

            guard granted else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.applicationIconBadgeNumber = 0
            })
            self.getNotificationSettings()
        }
        
        Messaging.messaging().delegate = self
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
        
//        let _ = T2PKycSdk.onFCMReceived(message: userInfo)
        
        if let id = userInfo["gcm.message_id"] as? String {
            fcmIdHadler(id: id)
        }
        
        if let aps = userInfo["aps"] as? [AnyHashable : AnyObject] {
            fcmApsHadler(aps: aps)
        }
        
        if let data = userInfo["gcm.notification.data"] as? [String : Any] {
            fcmDataHadler(data: data)
        }
    }
    
    private func fcmIdHadler(id: String) {
        NSLog("fcmIdHadler: id: \(id)")
    }
    
    private func fcmApsHadler(aps: [AnyHashable : Any]) {}
    
    private func fcmDataHadler(data: [String : Any]) {}
}

