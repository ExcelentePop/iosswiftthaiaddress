//
//  ViewController.swift
//  iOSSwiftThaiAddress
//
//  Created by nattaponpop on 03/22/2019.
//  Copyright (c) 2019 nattaponpop. All rights reserved.
//

import UIKit
//import ThaiAddressPack
//import 
//import ThaiAddress
import iOSSwiftThaiAddress
import ThaiAddress

class ViewController: UIViewController {
    
    @IBOutlet weak var tf: UITextField!
    
    lazy var address: ThaiAddress = {
        return ThaiAddress(bundle: iOSSwiftThaiAddress.bundle)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        address.preparingData {}
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        address.searchWith(text: self.tf.text!) {
            for item in self.address.searchResult {
                let th = item.th
                let en = item.en
                
                NSLog("addressData.searchResult: \(th.subDistrict), \(th.district), \(th.province), \(item.zipcode)")
                NSLog("addressData.searchResult: \(en.subDistrict), \(en.district), \(en.province), \(item.zipcode)")
                NSLog("addressData.searchResult: \(item.subDistrictCode), \(item.provinceCode) \n\n\n")
            }
        }
    }

}
